const path = require('path');
const CompressionPlugin = require('compression-webpack-plugin');

const config = {
  context: __dirname,
  entry: './src/main/resources/static/js/App.js',
  output: {
    path: path.join(__dirname, 'src/main/resources/static'),
    filename: 'bundle.js'
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader'
      },
      {
        test: /\.jsx?$/,
        exclude: /(node_modules)/,
        loader: 'eslint-loader'
      },
      {
        test: /\.css?$/,
        exclude: /(node_modules)/,
        loader: 'css-loader'
      }
    ]
  },
};

if (process.env.NODE_ENV === 'production') {
  config.plugins = [
    new CompressionPlugin({
      asset: '[path].gz[query]',
      algorithm: 'gzip',
      test: /\.(js|html)$/,
      threshold: 10240,
      minRatio: 0.8
    }),
  ];
}

module.exports = config;
