import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Form, Card, Message } from 'semantic-ui-react';
import Axios from 'axios';

export default class CreateUser extends React.Component {
	constructor() {
		super();
		this.state = {
			isLoading: false,
			showSuccess: false,
			showFailed: false,
			failedMsg: '',
			name: '',
			username: '',
			password: '',
			confirmPass: '',
		};
	}

	onCreateClick() {
		this.setState({
			isLoading: true,
			showSuccess: false,
			showFailed: false,
		});
		if(this.state.name === '' || this.state.username === '' || this.state.password === '' || this.state.confirmPass === '') {
			this.setState({
				isLoading: false,
				showFailed: true,
				failedMsg: 'Please fill in all fields.',
			});
		}
		else if(this.state.password !== this.state.confirmPass) {
			this.setState({
				isLoading: false,
				showFailed: true,
				failedMsg: 'Make sure both passwords are matching.',
			});
		}
		else {
			Axios.put('/api/user/create', {
				name: this.state.name,
				username: this.state.username,
				password: this.state.password
			})
			.then((response) => {
				this.setState({
					isLoading: false,
					showSuccess: true,
					name: '',
					username: '',
					password: '',
					confirmPass: '',
				});
			})
			.catch((error) => {
				this.setState({
					isLoading: false,
					failedMsg: 'Username is already taken. Please choose a different one.',
					showFailed: true,
				});
			})
		}
	}

	render() {
		return (
			<div
				style={{ marginTop: '6em',  }}>
				<Card
					centered={true}
					raised={true}
					fluid={true}
					style={{ maxWidth: '50em', minWidth: '10em', backgroundColor: 'ghostwhite'}}>
			    <Card.Content>
			      <Card.Header>
			        Create New User
			      </Card.Header>
					</Card.Content>
					<Card.Content>
			      <Card.Description>
						<Form
							loading={this.state.isLoading}
							size="big">
							<Form.Field
								required={true}>
								<label>Name</label>
								<input
									value={this.state.name}
									onChange={(e) => this.setState({name: e.target.value})} placeholder='Name' />
							</Form.Field>
							<Form.Field
								required={true}>
								<label>Username</label>
								<input
									value={this.state.username}
									onChange={(e) => this.setState({username: e.target.value})} placeholder='Username' />
							</Form.Field>
							<Form.Field
								required={true}>
								<label>Password</label>
								<input
									type='password'
									value={this.state.password}
									onChange={(e) => this.setState({password: e.target.value})} placeholder='Password' />
							</Form.Field>
							<Form.Field
								required={true}>
								<label>Confirm your password</label>
								<input
									value={this.state.confirmPass}
									onChange={(e) => this.setState({ confirmPass: e.target.value})}
									type='password' placeholder='Confirm your password' />
							</Form.Field>
							<Message
								size="mini"
								hidden={!this.state.showSuccess}
								positive
								header='Successfully created User!'
							/>
							<Message
								size="mini"
								hidden={!this.state.showFailed}
								negative
								header='Failed to create User.'
								content={this.state.failedMsg}
							/>
							<Form.Group inline>
								<Button
									onClick={() => this.onCreateClick()}
									primary type='submit'>Create New User
								</Button>
								<Link to="/Login">
									<label
										style= {{ textAlign: 'right', cursor: 'pointer'}}>
										Go back to Login.
									</label>
								</Link>
							</Form.Group>
						</Form>
			      </Card.Description>
			    </Card.Content>
			  </Card>
			</div>
		);
	}
}
