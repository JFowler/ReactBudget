import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';
import './../main.css';

import Home from './Home';
import Login from './Login';
import CreateUser from './CreateUser';
import Nav from './Nav';
import Transaction from './Transaction';
import Error from './Error';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      user: {
        isAuthenticated: false,
        name: '',
        username: '',
        id: '',
      },
    };
  }

  authenticateUser(newUser) {
    this.setState({
      user: {
        ...this.state.user,
        isAuthenticated: true,
        name: newUser.name,
        username: newUser.username,
        id: newUser.id,
      },
    });
  }

  logout() {
    this.setState({
      user: {
        isAuthenticated: false,
        name: '',
        username: '',
        id: '',
      },
    });
  }

  render() {
    const AuthRoute = ({ component, ...rest }) => (
      <div>
      <Nav logout={() => this.logout()} user={this.state.user} />
      <Route
        {...rest}
        render={props => (
          this.state.user.isAuthenticated
          ? React.createElement(component, props)
          : <Redirect to={{ pathname: '/Login' }} />
        )}
      />
      </div>
    )

    const NonAuthRoute = ({ component, ...rest }) => (
     <Route
       {...rest}
       render={props => (
         !this.state.user.isAuthenticated
         ? React.createElement(component, props)
         : <Redirect to={{ pathname: '/Home' }} />
       )}
     />
   );

    return (
        <BrowserRouter>
          <div>

            <Switch>
              <AuthRoute path="/Home" component={() => <Home user={this.state.user} />} />
              <AuthRoute path="/Transaction" component={Transaction} />
              <NonAuthRoute
                path="/Login"
                component={() => <Login authenticateUser={(newUser) => this.authenticateUser(newUser)} />}
              />
              <Route path="/CreateUser" component={CreateUser} />
              <Route path="/error" component={Error} />
            </Switch>
          </div>
        </BrowserRouter>
    );
  }
}


ReactDOM.render(
  <App />,
	  document.getElementById('react'),
);
