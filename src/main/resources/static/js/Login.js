import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Button, Form, Checkbox, Card, Message } from 'semantic-ui-react';
import Axios from 'axios';


export default class Login extends React.Component {
	constructor() {
		super();
		this.state = {
				isLoading: false,
				showFailed: false,
				failedMsg: '',
				username: '',
				password: '',
		};
	}

	onLoginClick() {
		this.setState({
			isLoading: true,
			showFailed: false,
		});

		if(this.state.username === '') {
			this.setState({
				failedMsg: 'Please fill in your Username.',
				isLoading: false,
				showFailed: true,
			});
		}
		else if(this.state.password === '') {
			this.setState({
				failedMsg: 'Please fill in your Password.',
				isLoading: false,
				showFailed: true,
			});
		}
		else {
			Axios.post('/api/user/login', {
				username: this.state.username,
				password: this.state.password
			})
			.then((response) => {
				this.props.authenticateUser(response.data);
			})
			.catch((error) => {
				this.setState({
					failedMsg: 'Make sure your Username and Password are correct.',
					isLoading: false,
					showFailed: true,
				});
			})
		}
	}

	render() {
		return (
			<div
				style={{ marginTop: '6em',  }}>
				<Card
					centered={true}
					raised={true}
					fluid={true}
					style={{ maxWidth: '50em', minWidth: '10em', backgroundColor: 'ghostwhite'}}>
			    <Card.Content>
			      <Card.Header>
			        React Budget
			      </Card.Header>
					</Card.Content>
					<Card.Content>
			      <Card.Description>
							<Form
								success
								loading={this.state.isLoading}
								size="big">
								<Form.Field
									required={true}>
									<label>Username</label>
									<input
										value={this.state.username}
										onChange={(e) => this.setState({username: e.target.value})} placeholder='Username' />
								</Form.Field>
								<Form.Field
									required={true}>
									<label>Password</label>
									<input
										type='password'
										value={this.state.password}
										onChange={(e) => this.setState({password: e.target.value})} placeholder='Password' />
								</Form.Field>
								<Message
									size="mini"
									hidden={!this.state.showFailed}
						      negative
						      header='Failed to log in.'
						      content={this.state.failedMsg}
						    />
								<Form.Group inline>
									<Button
										onClick={() => this.onLoginClick()}
										primary type='submit'>Login
									</Button>
									<Link to="/CreateUser">
										<label
											style= {{ textAlign: 'right', cursor: 'pointer'}}>
											Create an Account!
										</label>
									</Link>
								</Form.Group>
							</Form>
			      </Card.Description>
			    </Card.Content>
			  </Card>
			</div>
		);
	}
}

Login.propTypes = {
	authenticateUser: PropTypes.func,
};
