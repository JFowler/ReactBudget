import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, Image, Modal, Form, Message } from 'semantic-ui-react';
import Axios from 'axios';


export default class Home extends React.Component {
  constructor(props) {
    super(props);
    Axios.get('/api/budget/all/' + props.user.id, {})
    .then((response) => {
      this.setState({ budgets: response.data})
    })

    this.state = {
      isCreateLoading: false,
      budgets: [],
      openBudgetModal: false,
      budgetName: '',
      budgetGoal: '',
      openTransactionModal: false,
      sendBudget: {},
      transactionCost: '',
      transactionLoading: false,
      deleteLoading: false,
      failedBudgetMsg: '',
      failedTransMsg: '',
      showBudgetFailed: false,
      showTransFailed: false,
    };
  }

  createNewBudget() {
    this.setState({ isCreateLoading: true, showBudgetFailed: false})
    if(this.state.budgetName === '') {
      this.setState({
        isCreateLoading: false,
        failedBudgetMsg: 'Please add a name.',
        showBudgetFailed: true,
      })
    }
    else {
      Axios.post('/api/budget/create', {
        name: this.state.budgetName,
        totalBudget: this.state.budgetGoal,
      })
      .then((response) => {
        Axios.get('/api/budget/all/' + this.props.user.id, {})
        .then((response) => {
          this.setState({ budgets: response.data})
        })
        this.setState({
          openBudgetModal: false,
          budgetName: '',
          budgetGoal: '',
          isCreateLoading: false,
        })
      })
      .catch((error) => {
        this.setState({
          isCreateLoading: false,
          failedBudgetMsg: 'Could not create a Budget, Please try again.',
          showBudgetFailed: true,
        })
      })
    }
  }

  deleteBudget(id) {
    this.setState({ deleteLoading: true })
    Axios.delete('/api/budget/delete/' + id, {})
    .then((response) => {
      Axios.get('/api/budget/all/' + this.props.user.id, {})
      .then((response) => {
        this.setState({ budgets: response.data, deleteLoading: false})
      })
    })
  }

  addTransaction() {
    this.setState({ transactionLoading: true, showTransFailed: false, })
    if(this.state.transactionCost === '') {
      this.setState({
        failedTransMsg: 'Please add a cost.',
        showTransFailed: true,
        transactionLoading: false,
      })
    }
    else {
      Axios.put('/api/transaction/create', {
        cost: this.state.transactionCost,
        budget: this.state.sendBudget,
      })
      .then((response) => {
        for(var i=0;i<this.state.budgets.length;i++) {
          if(this.state.budgets[i].id === this.state.sendBudget.id) {
            var spent = Number.parseInt(this.state.budgets[i].totalSpent)
            var cost = Number.parseInt(this.state.transactionCost)
            this.state.budgets[i].totalSpent = spent + cost;
          }
        }
        this.setState({ transactionLoading: false, openTransactionModal: false, transactionCost: '', sendBudget: {}})
      })
      .catch((error) => {
        this.setState({
          failedTransMsg: 'Could not create a transaction, Please try again.',
          showTransFailed: true,
          transactionLoading: false,
        })
      })
    }
  }

  render() {
    var budgetCards = this.state.budgets.map((item) => {
      return (
        <Card
          raised={true}
          color={'blue'}
          style={{ backgroundColor: 'ghostwhite' }}
          key={item.id}>
          <Card.Content>
          <Button
            loading={this.state.deleteLoading}
            onClick={() => this.deleteBudget(item.id)}
            style={{ float: 'right' }}
            inverted color="red"
            size='mini'>X
          </Button>
            <Card.Header>
              <h2>{item.name}</h2>
            </Card.Header>
            <Card.Meta>
              Goal: ${item.totalBudget}
            </Card.Meta>
            <Card.Description>
              <h3>Spent: ${item.totalSpent}</h3>
            </Card.Description>
          </Card.Content>
          <Card.Content extra>
            <div className='ui two buttons'>
              <Button
                onClick={() => this.setState({ openTransactionModal: true, sendBudget: item})}
                inverted color='blue'>Add Transaction</Button>
            </div>
          </Card.Content>
        </Card>
      );
    });


    return (
      <div>
        <Card.Group
          style={{ marginLeft: '3em', marginTop: '3em'}}>
          {budgetCards}
          <Card
            raised={true}
            color={'blue'}
            onClick={() => this.setState({ openBudgetModal: true})}
            style={{ textAlign: 'center', backgroundColor: 'ghostwhite' }}>
            <Card.Content>
              <Card.Header>
                <Image size='small' src='./icon/addgreen.png' />
              </Card.Header>
            </Card.Content>
          </Card>
        </Card.Group>


        <Modal
          open={this.state.openBudgetModal}
          onClose={() => this.setState({openBudgetModal: false, budgetName: '', budgetGoal: '' })}>
          <Modal.Header>Create new Budget</Modal.Header>
          <Modal.Content>
            <Modal.Description>
            <Form
              loading={this.state.isCreateLoading}>
              <Form.Field
                required={true}>
                <label>Budget name</label>
                <input
                  value={this.state.budgetName}
                  onChange={(e) => this.setState({ budgetName: e.target.value})}
                  placeholder='Budget name' />
              </Form.Field>
              <Form.Field>
                <label>Budget Goal</label>
                <input
                  value={this.state.budgetGoal}
                  onChange={(e) => this.setState({ budgetGoal: e.target.value})}
                  type='number' placeholder='EX: 200' />
              </Form.Field>
              <Message
                hidden={!this.state.showBudgetFailed}
                negative
                header='Failed to create Budget.'
                content={this.state.failedBudgetMsg}
              />
              <Form.Group inline>
                <Button
                  onClick={() => this.createNewBudget()}
                  basic color='green' type='submit'>Submit</Button>
                <Button
                  onClick={() => this.setState({ openBudgetModal: false, budgetName: '', budgetGoal: ''  })}
                  basic color='red' type='button'>Cancel</Button>
              </Form.Group>
            </Form>
            </Modal.Description>
          </Modal.Content>
        </Modal>

        <Modal
          open={this.state.openTransactionModal}
          onClose={() => this.setState({openTransactionModal: false, transactionCost: '', })}>
          <Modal.Header>Add transaction to {this.state.sendBudget.name} </Modal.Header>
          <Modal.Content>
            <Modal.Description>
            <Form
              loading={this.state.transactionLoading}>
              <Form.Field
                required={true}>
                <label>Cost</label>
                <input
                  value={this.state.transactionCost}
                  onChange={(e) => this.setState({ transactionCost: e.target.value})}
                  type='number' placeholder='EX: 10.54' />
              </Form.Field>
              <Message
                hidden={!this.state.showTransFailed}
                negative
                header='Failed to add Transaction'
                content={this.state.failedTransMsg}
              />
              <Form.Group inline>
                <Button
                  onClick={() => this.addTransaction()}
                  basic color='green' type='submit'>Add Transaction</Button>
                <Button
                  onClick={() => this.setState({ openTransactionModal: false, transactionCost: '', })}
                  basic color='red' type='button'>Cancel</Button>
              </Form.Group>
            </Form>
            </Modal.Description>
          </Modal.Content>
        </Modal>
      </div>
    );
  }
}
