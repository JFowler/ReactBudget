import React, { Component } from 'react'
import { Input, Menu, Dropdown } from 'semantic-ui-react'
import { Link } from 'react-router-dom';

export default class MenuExampleSecondary extends Component {
  constructor() {
    super();
    this.state = {
      activeItem: 'home',
    };
  }


  handleItemClick(name) {
    this.setState({ activeItem: name });
    console.log(this.props.logout)
    console.log(this.props.user)
  }

  render() {
    const { activeItem } = this.state

    return (
      <Menu pointing
        size="huge"
        color="blue">
        <Link to='/Home'><Menu.Item as='div' name='Home' active={activeItem === 'Home'} onClick={() => this.handleItemClick('Home')} /> </Link>
        <Link to='/Transaction'><Menu.Item as='div' name='Transactions' active={activeItem === 'Transaction'} onClick={() =>this.handleItemClick('Transaction')} /> </Link>
        <Menu.Menu as='div' position='right'>
          <Dropdown text={this.props.user.name} pointing className='link item'>
            <Dropdown.Menu>
              <Dropdown.Item>Profile</Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item active={activeItem === 'logout'} onClick={() => this.props.logout()}>Logout</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Menu.Menu>
      </Menu>
    )
  }
}
