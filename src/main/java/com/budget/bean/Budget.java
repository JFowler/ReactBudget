package com.budget.bean;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "budget")
public class Budget {
	@Id
	@Column(name = "BUDGETID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "budget_seq")
	@SequenceGenerator(name = "budget_seq", sequenceName = "budget_seq")
	private long id;
	private String name;
	@Column(name = "TOTALSPENT")
	private double totalSpent;
	@Column(name = "TOTALBUDGET")
	private double totalBudget;
	
	@ManyToOne
	@JoinColumn(name = "USERID")
//	@JsonIgnore
	private User user;
	
	@OneToMany(mappedBy = "budget", fetch = FetchType.LAZY,  cascade = CascadeType.ALL)
	@JsonIgnore
	private Collection<Transaction> transaction;

	public Budget(long id, String name, double totalSpent, double totalBudget, User user,
			Collection<Transaction> transaction) {
		super();
		this.id = id;
		this.name = name;
		this.totalSpent = totalSpent;
		this.totalBudget = totalBudget;
		this.user = user;
		this.transaction = transaction;
	}

	public Budget() {
		super();
	}

	public long getid() {
		return id;
	}

	public void setid(long id) {
		this.id = id;
	}

	public double getTotalSpent() {
		return totalSpent;
	}

	public void setTotalSpent(double totalSpent) {
		this.totalSpent = totalSpent;
	}

	public double getTotalBudget() {
		return totalBudget;
	}

	public void setTotalBudget(double totalBudget) {
		this.totalBudget = totalBudget;
	}

	@JsonIgnore
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Collection<Transaction> getTransaction() {
		return transaction;
	}

	public void setTransaction(Collection<Transaction> transaction) {
		this.transaction = transaction;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		long temp;
		temp = Double.doubleToLongBits(totalBudget);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(totalSpent);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((transaction == null) ? 0 : transaction.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Budget other = (Budget) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Double.doubleToLongBits(totalBudget) != Double.doubleToLongBits(other.totalBudget))
			return false;
		if (Double.doubleToLongBits(totalSpent) != Double.doubleToLongBits(other.totalSpent))
			return false;
		if (transaction == null) {
			if (other.transaction != null)
				return false;
		} else if (!transaction.equals(other.transaction))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Budget [id=" + id + ", name=" + name + ", totalSpent=" + totalSpent + ", totalBudget="
				+ totalBudget + ", user=" + user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
