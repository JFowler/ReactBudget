package com.budget.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.budget.bean.User;
import com.budget.service.UserService;

@RestController
@RequestMapping("/api/user")
public class UserController {
	
	@Autowired
	private UserService uService;
	
	@PostMapping("/login")
	public User login(@RequestBody User user, HttpServletResponse resp, HttpServletRequest req) {
		User u = uService.login(user);
		if(u != null) {
			req.getSession().setAttribute("user", u);
			return u;
		}
		else {
			resp.setStatus(400);
			return null;
		}
	}
	
	@PutMapping("/create")
	public void create(@RequestBody User user, HttpServletResponse resp) {
		if(uService.checkAndCreateUser(user)) 
			resp.setStatus(201);
		else
			resp.setStatus(400);
	}
}
