package com.budget.rest;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.budget.bean.Budget;
import com.budget.bean.User;
import com.budget.service.BudgetService;

@RestController
@RequestMapping("/api/budget")
public class BudgetController {
	@Autowired
	BudgetService bService;
	
	@GetMapping("/all/{userId}")
	public Collection<Budget> getAll(@PathVariable long userId, HttpServletResponse resp, HttpServletRequest req) {
		return bService.getAll(userId);
	}
	
	@PostMapping("/create")
	public void create(@RequestBody Budget b, HttpServletResponse resp, HttpServletRequest req) {
		b.setUser((User) req.getSession().getAttribute("user"));
		bService.create(b);
	}
	
	@DeleteMapping("/delete/{id}")
	public void delete(@PathVariable long id, HttpServletResponse resp, HttpServletRequest req) {
		bService.delete(id);
	}
}
