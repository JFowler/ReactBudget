package com.budget.rest;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.budget.bean.Transaction;
import com.budget.service.TransactionService;

@RestController
@RequestMapping("/api/transaction")
public class TransactionController {
	@Autowired
	private TransactionService tService;
	
	@PutMapping("/create")
	public void create(@RequestBody Transaction trans, HttpServletRequest req) {
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		trans.setDate(date);
		
		tService.create(trans);
	}
	
}
