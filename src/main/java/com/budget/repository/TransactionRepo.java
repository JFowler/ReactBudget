package com.budget.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.budget.bean.Transaction;

public interface TransactionRepo extends JpaRepository<Transaction, Long> {

}
