package com.budget.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.budget.bean.User;

public interface UserRepo extends JpaRepository<User, Long> {
	User getById(long id);
	User getByUsernameAndPassword(String username, String password);
	User getByUsername(String username);
}
