package com.budget.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.budget.bean.Budget;

@Repository
public interface BudgetRepo extends JpaRepository<Budget, Long>{
	Collection<Budget> findByUserId(long id);
	void deleteById(long id);
	Budget findById(long id);
}
