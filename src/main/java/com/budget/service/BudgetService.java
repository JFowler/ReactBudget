package com.budget.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.bean.Budget;
import com.budget.repository.BudgetRepo;

@Service
public class BudgetService {
	@Autowired
	private BudgetRepo bRepo;
	
	public Collection<Budget> getAll(long id) {
		return bRepo.findByUserId(id);
	}
	
	public void create(Budget b) {
		bRepo.saveAndFlush(b);
	}
	
	public void delete(long id) {
		Budget b = bRepo.findById(id);
		bRepo.delete(b);
	}
}
