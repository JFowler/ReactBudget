package com.budget.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.bean.Budget;
import com.budget.bean.Transaction;
import com.budget.repository.BudgetRepo;
import com.budget.repository.TransactionRepo;

@Service
public class TransactionService {
	@Autowired
	private TransactionRepo tRepo;
	
	@Autowired
	private BudgetRepo bRepo;
	
	public void create(Transaction trans) {
		Budget b = bRepo.findById(trans.getBudget().getid());
		b.setTotalSpent(trans.getCost() + b.getTotalSpent());
		
		tRepo.saveAndFlush(trans);
		bRepo.saveAndFlush(b);
	}
}
