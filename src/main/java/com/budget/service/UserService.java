package com.budget.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budget.bean.User;
import com.budget.repository.UserRepo;

@Service
public class UserService {
	@Autowired
	private UserRepo uRepo;
	
	public User login(User u) {
		return uRepo.getByUsernameAndPassword(u.getUsername(), u.getPassword());
	}
	
	public boolean checkAndCreateUser(User u) {
		User checkUser = uRepo.getByUsername(u.getUsername());
		if(checkUser == null) {
			uRepo.saveAndFlush(u);
			return true;
		}
		else 
			return false;
	}
}
