package com.budget.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
//@RequestMapping("/")
public class SPAControllerImpl {

	@RequestMapping(value ="/Home", method = RequestMethod.GET)
	public String getHome() {
		return "index.html";
	}
	
	@RequestMapping(value ="/Login", method = RequestMethod.GET)
	public String getLogin() {
		return "index.html";
	}
	
	@RequestMapping(value ="/CreateUser", method = RequestMethod.GET)
	public String getCreateUser() {
		return "index.html";
	}

}
