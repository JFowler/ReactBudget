package com.budget.controller;

import org.springframework.stereotype.Controller;

@Controller
public interface SPAController {

	public String getSpa();

}
