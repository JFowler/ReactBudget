DROP USER reactBudget cascade;

CREATE USER reactBudget
IDENTIFIED BY p4ssw0rd
DEFAULT TABLESPACE users
TEMPORARY TABLESPACE temp
QUOTA 10M ON users;

GRANT connect to reactBudget;
GRANT resource to reactBudget;
GRANT create session TO reactBudget;
GRANT create table TO reactBudget;
GRANT create view TO reactBudget;

create table reactBudget.budgetUser
(
    userid Number(19) primary key,
    name VarChar2(30) not null,
    username VarChar2(20) not null,
    password VarChar2(20) not null
);

create table reactBudget.budget
(
    budgetid Number(19) primary key,
    name VarChar2(20) not null,
    userid Number(19) not null,
    FOREIGN KEY (userid) REFERENCES reactBudget.budgetUser (userid),
    totalSpent Number(10),
    totalBudget Number(10)
);

create table reactBudget.budgetTransaction
(
    transactionid Number(19) primary key,
    budgetid Number(19) not null,
    FOREIGN KEY (budgetid) REFERENCES reactBudget.budget (budgetid),
    transactionDate Date not null,
    cost Number(10,2) not null
);

create sequence reactBudget.user_seq;
create sequence reactBudget.budget_seq;
create sequence reactBudget.transaction_seq;